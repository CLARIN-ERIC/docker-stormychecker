#!/bin/bash

source "${DATA_ENV_FILE:-copy_data.env.sh}"


init_data (){
	STORMY_GIT_DIRECTORY="$(pwd)/stormy-src"
    
    cleanup_data

    if [ -z ${DEV_LOCATION} ]
    then     
	mkdir -p "${STORMY_GIT_DIRECTORY}"
        (
	  		TMP_TGZ=$(mktemp)
			cd "${STORMY_GIT_DIRECTORY}" && 
				wget -O "${TMP_TGZ}" "${STORMY_REPO_URL}/archive/${STORMY_BRANCH}.tar.gz" &&
				tar zxvf "${TMP_TGZ}" --strip-components 1 &&
				rm "${TMP_TGZ}"
        )
    else
        cp -r $DEV_LOCATION $CURATION_GIT_DIRECTORY
    fi
    
}

cleanup_data () {
    if [ -d "${STORMY_GIT_DIRECTORY}" ]; then
    	echo "Cleaning up source directory ${STORMY_GIT_DIRECTORY}"
	    rm -rf "${STORMY_GIT_DIRECTORY}"
	fi
}
