#!/usr/bin/env bash
DB_SERVICE="stormcrawler-db"

DB_USER=stormcrawler
DB_PASSWORD=stormcrawler
DB_NAME=stormychecker
DB_PORT=3306

SCRIPT="$1"

if ! [ "${SCRIPT}" ]; then
	echo "Usage: $0 script-file"
	exit 1
fi

echo "Stopping all services"
docker-compose down
echo "Starting database service"
DB_CONTAINER="$(docker-compose run -d -v "${SCRIPT}":/tmp/script.sql "${DB_SERVICE}")"
while ! docker ps|grep "${DB_CONTAINER}"|grep "Up"; do
	echo "Waiting for state of container ${DB_CONTAINER} to be UP"
	sleep 1
done
docker exec "${DB_CONTAINER}" /bin/sh -c "while ! mysqladmin ping -h localhost --silent; do echo Waiting for mysql...; sleep 1; done && echo Executing && mysql -v --user=${DB_USER} --password=${DB_PASSWORD} --host=localhost --port=${DB_PORT} ${DB_NAME} < /tmp/script.sql"
docker-compose down
