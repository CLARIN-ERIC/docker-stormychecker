# docker-stormychecker

This project builds [the Stormychecker application](https://github.com/acdh-oeaw/stormychecker) into a Docker image, 
which can then be used to deploy it on Clarin Servers. The deployment is taken care of by the [compose_curation_module_stormychecker](https://gitlab.com/CLARIN-ERIC/compose_curation_module_stormychecker) project.

## Build
The image build process builds stormychecker from sources. Sources are retrieved from
GitHub, see copy_data.sh.

### Common use case: Building and Releasing after changing code
1. Implement a new feature in Stormychecker code base, afterwards run:
    1. `git add --all`
    2. `git commit -m "implement new feature"`
    3. `git tag -a {version_tag1} -m "implement new feature"`
    4. `git push origin {version_tag1}`
2. In the docker-stormychecker project update the `STORMY_BRANCH` to {version_tag1} in the `copy_data.env.sh` file, then run: 
    1. `git add --all`
    2. `git commit -m "update cm version"`
    3. `git tag -a {version_tag2} -m "update cm version"`
    4. `git push origin {version_tag2}` 
    
    This will trigger a pipeline on Gitlab for building and pushing to docker registry.
    
### Gitlab
Gitlab is set up via the .gitlab-ci.yml file to:
   * build every push.
   * build every push of a tag and release (push the Docker image with the same tag to clarin Docker registry)